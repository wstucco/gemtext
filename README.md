# Gemtext

Gemtext is a lightweight markup language taht powers the Gemini ecosystem.

The [official gemtext specs](https://gemini.circumlunar.space/docs/gemtext.gmi) explain the language
in details, if you are in a rush, you can refer to the [gemtext cheatsheet](https://gemini.circumlunar.space/docs/cheatsheet.gmi).

This library can parse a "gemtext" into a list of nodes or render the samne list of nodes into
the original "gemtext".

### Example

```elixir
gemtext = """
# Heading 1
## Heading 2
### Heading 3

> Quotation Line 1
> Quotation Line 2

This is normal text Line 1
This is normal text Line 2
This is a robot emoji 🤖

=> gemini://gemini.circumlunar.space This is a link
=> /naked_link
"""

blocks = Gemtext.parse(gemtext)
[
  {:h1, "Heading 1"},
  {:h2, "Heading 2"},
  {:h3, "Heading 3"},
  {:text, ""},
  {:quote, "Quotation Line 1"},
  {:quote, "Quotation Line 2"},
  {:text, ""},
  {:text, "This is normal text Line 1"},
  {:text, "This is normal text Line 2"},
  {:text, "This is a robot emoji 🤖"},
  {:text, ""},
  {:link, %{to: "gemini://gemini.circumlunar.space"}, "This is a link"},
  {:link, %{to: "/naked_link"}, nil},
  {:text, ""},
]

Gemtext.renderer(blocks) |> IO.puts() 
# Heading 1
## Heading 2
### Heading 3

> Quotation Line 1
> Quotation Line 2

This is normal text Line 1
This is normal text Line 2
This is a robot emoji 🤖

=> gemini://gemini.circumlunar.space This is a link
=> /link_no_body

```

## Installation

This package can be installed by adding `gemtext` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:gemtext, "~> 0.2.0"}
  ]
end
```

The are available at <https://hexdocs.pm/gemtext>.

## License

Licensed under the The GNU General Public License v3.0 (GPL-3.0) https://www.gnu.org/licenses/gpl-3.0.en.html.

