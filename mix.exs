defmodule Gemtext.MixProject do
  use Mix.Project

  @version "0.2.0"

  @name "Gemtext"
  @description "Gemtext is a pure Elixir Gemini Gemtext markup parser and converter"
  @repo_url "https://gitlab.com/wstucco/gemtext/"
  @doc_url "https://hexdocs.pm/gemtext/"

  def project do
    [
      app: :gemtext,
      version: @version,
      version: "0.1.0",
      elixir: "~> 1.13",
      start_permanent: Mix.env() == :prod,
      consolidate_protocols: Mix.env() != :test,
      deps: deps(),
      name: @name,
      package: package(),

      # Docs
      source_url: @repo_url,
      homepage_url: @doc_url,
      description: @description,
      docs: docs()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  defp package do
    [
      maintainers: ["Massimo Ronca <mr@massimoronca.it>"],
      licenses: ["GPL-3.0-or-later"],
      files: ~w(lib .formatter.exs .gitignore mix.exs README.md LICENSE CHANGELOG),
      links: %{
        "Repo" => @repo_url,
        "Gemtext specs" => "https://gemini.circumlunar.space/docs/gemtext.gmi",
        "Gemtext cheatsheet" => "https://gemini.circumlunar.space/docs/cheatsheet.gmi"
      }
    ]
  end

  defp docs do
    [
      authors: [
        "Massimo Ronca <mr@massimoronca.it>"
      ],
      # main: "index",
      formatters: ["html"],
      source_url_patter: "https://gitlab.com/wstucco/gemtext/-/blob/main/%{path}#L%{line}"
    ]
  end

  def deps() do
    [
      {:nimble_parsec, "~> 1.2"},
      {:credo, "~> 1.6", only: :dev, runtime: false},
      {:ex_doc, "~> 0.29", only: :dev, runtime: false}
    ]
  end
end
