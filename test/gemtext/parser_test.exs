defmodule Gemtext.ParserTest do
  use ExUnit.Case
  doctest Gemtext.Parser

  import Gemtext.Parser

  test "parse text" do
    gemtext = "hi\nthere\n    my friends"
    expected = [text: "hi", text: "there", text: "    my friends"]
    nodes = parse(gemtext)
    assert nodes == expected
  end

  test "parse link" do
    gemtext = "=>/\n=> / Go home"
    expected = [{:link, %{to: "/"}, nil}, {:link, %{to: "/"}, "Go home"}]
    nodes = parse(gemtext)
    assert nodes == expected
  end

  test "parse link with random spaces" do
    gemtext = "=>   /\n=>    /     Go home"
    expected = [{:link, %{to: "/"}, nil}, {:link, %{to: "/"}, "Go home"}]
    nodes = parse(gemtext)
    assert nodes == expected
  end

  test "parse quote" do
    gemtext = ">hi there\n> hi there\n>  hi there"
    expected = [quote: "hi there", quote: "hi there", quote: " hi there"]
    nodes = parse(gemtext)
    assert nodes == expected
  end

  test "parse headers" do
    gemtext = "#hi\n##there\n### my friends"
    expected = [h1: "hi", h2: "there", h3: "my friends"]
    nodes = parse(gemtext)
    assert nodes == expected
  end

  test "parse list items" do
    gemtext = "* hi\n*there\n*    my friends"
    expected = [li: "hi", li: "there", li: "   my friends"]
    nodes = parse(gemtext)
    assert nodes == expected
  end

  test "parse preformatted text" do
    gemtext = "```hi there\nmy friend\nobi-wan kenobi\n```\n\nTest"

    expected = [
      {:pre, %{title: "hi there"}, "my friend\nobi-wan kenobi\n"},
      text: "",
      text: "Test"
    ]

    nodes = parse(gemtext)
    assert nodes == expected
  end

  test "parse preformatted text when is not the first element" do
    gemtext = "# HI\n```\nhi there\nmy friend\nobi-wan kenobi\n```\n\nTest"

    expected = [
      h1: "HI",
      pre: "hi there\nmy friend\nobi-wan kenobi\n",
      text: "",
      text: "Test"
    ]

    nodes = parse(gemtext)
    assert nodes == expected
  end

  test "parse ambigous preformatted text" do
    gemtext =
      """
      ``` foo
      FOO
      ```
      Foo bar
      """
      |> String.trim_trailing("\n")

    expected = [
      {:pre, %{title: "foo"}, "FOO\n"},
      text: "Foo bar"
    ]

    nodes = parse(gemtext)
    assert nodes == expected
  end

  test "parse ambigous nested preformatted text" do
    gemtext =
      """
      ``` foo
       ```FOO
      ```
      Foo bar
      """
      |> String.trim_trailing("\n")

    expected = [{:pre, %{title: "foo"}, " ```FOO\n"}, text: "Foo bar"]

    nodes = parse(gemtext)
    assert nodes == expected
  end

  test "unbalanced preformatted text" do
    gemtext =
      """
      ``` foo
      Foo bar
      """
      |> String.trim_trailing("\n")

    expected = [text: "``` foo", text: "Foo bar"]

    result = parse(gemtext)
    assert result == expected
  end

  test "correctly process last empty line" do
    gemtext = "Test\n"
    expected = [text: "Test", text: ""]

    nodes = parse(gemtext)
    assert nodes == expected
  end
end
