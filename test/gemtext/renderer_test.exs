defmodule Gemtext.RendererTest do
  use ExUnit.Case
  doctest Gemtext.Renderer

  import Gemtext.Renderer

  test "render text" do
    gemtext = "hi\nthere\n    my friends"
    blocks = [text: "hi", text: "there", text: "    my friends"]

    assert gemtext == render(blocks)
  end

  test "render link" do
    gemtext = "=> /\n=> / Go home"
    blocks = [{:link, %{to: "/"}, nil}, {:link, %{to: "/"}, "Go home"}]

    assert gemtext == render(blocks)
  end

  test "render quote" do
    gemtext = "> hi there"
    blocks = [quote: "hi there"]

    assert gemtext == render(blocks)
  end

  test "render headers" do
    gemtext = "# hi\n## there\n### my friends"
    blocks = [h1: "hi", h2: "there", h3: "my friends"]

    assert gemtext == render(blocks)
  end

  test "render list items" do
    gemtext = "* hi\n* there\n*    my friends"
    blocks = [li: "hi", li: "there", li: "   my friends"]

    assert gemtext == render(blocks)
  end

  test "render preformatted text" do
    gemtext = "``` hi there\nmy friend\nobi-wan kenobi\n```\n\nTest"

    blocks = [
      {:pre, %{title: "hi there"}, "my friend\nobi-wan kenobi\n"},
      text: "",
      text: "Test"
    ]

    assert gemtext == render(blocks)
  end

  test "render preformatted text when is not the first element" do
    gemtext = "# HI\n```\nhi there\nmy friend\nobi-wan kenobi\n```\n\nTest"

    blocks = [
      h1: "HI",
      pre: "hi there\nmy friend\nobi-wan kenobi\n",
      text: "",
      text: "Test"
    ]

    assert gemtext == render(blocks)
  end

  test "render ambigous preformatted text" do
    gemtext =
      """
      ``` foo
      FOO
      ```
      Foo bar
      """
      |> String.trim_trailing("\n")

    blocks = [
      {:pre, %{title: "foo"}, "FOO\n"},
      text: "Foo bar"
    ]

    assert gemtext == render(blocks)
  end

  test "render ambigous nested preformatted text" do
    gemtext =
      """
      ``` foo
       ```FOO
      ```
      Foo bar
      """
      |> String.trim_trailing("\n")

    blocks = [{:pre, %{title: "foo"}, " ```FOO\n"}, text: "Foo bar"]

    assert gemtext == render(blocks)
  end

  test "unbalanced preformatted text" do
    gemtext =
      """
      ``` foo
      Foo bar
      """
      |> String.trim_trailing("\n")

    blocks = [text: "``` foo", text: "Foo bar"]

    assert gemtext == render(blocks)
  end

  test "correctly process last empty line" do
    gemtext = "Test\n"
    blocks = [text: "Test", text: ""]

    assert gemtext == render(blocks)
  end
end
