defmodule Gemtext.Parser do
  @moduledoc """
  Gemtext parser
  """

  import NimbleParsec
  import Gemtext.Parser.Base
  alias Gemtext.Parser.{Headers, Li, Blockquote, Link, Pre, Text}

  @type tag :: atom()
  @type attributes :: map()
  @type content :: String.t()
  @type gemtext_block ::
          {tag(), attributes(), content()}
          | {tag(), content()}
  @type gemtext_blocks :: [gemtext_block()]

  types =
    choice([
      Headers.parser(),
      Li.parser(),
      Blockquote.parser(),
      Link.parser(),
      Pre.parser(),
      Text.parser()
    ])

  gemtext =
    concat(types, ignore(eol()))
    |> repeat()
    |> concat(types)
    |> eos()

  defparsecp(:do_parse, gemtext, inline: true)

  @doc ~S'''
  Parses a string of gemtext into a list of gemtext blocks

  *Note that empty lines are kept as `:text` blocks with no content
  as per specification.*

  ## Examples

      iex> Gemtext.Parser.parse("""
      ...> # Heading 1
      ...> ## Heading 2
      ...> ### Heading 3
      ...>
      ...> > Quotation Line 1
      ...> > Quotation Line 2
      ...>
      ...> This is normal text Line 1
      ...> This is normal text Line 2
      ...> This is a robot emoji 🤖
      ...>
      ...> => gemini://gemini.circumlunar.space This is a link
      ...> => /naked_link
      ...>
      ...> * Bullet 1
      ...> * Bullet 2
      ...> * Bullet 3
      ...>
      ...> ``` title
      ...>  _______________________
      ...> #                       #
      ...> | This is  preformatted |
      ...> |  text with two lines  |
      ...> #_______________________#
      ...> ```
      ...> """)
      [
        {:h1, "Heading 1"},
        {:h2, "Heading 2"},
        {:h3, "Heading 3"},
        {:text, ""},
        {:quote, "Quotation Line 1"},
        {:quote, "Quotation Line 2"},
        {:text, ""},
        {:text, "This is normal text Line 1"},
        {:text, "This is normal text Line 2"},
        {:text, "This is a robot emoji 🤖"},
        {:text, ""},
        {:link, %{to: "gemini://gemini.circumlunar.space"}, "This is a link"},
        {:link, %{to: "/naked_link"}, nil},
        {:text, ""},
        {:li, "Bullet 1"},
        {:li, "Bullet 2"},
        {:li, "Bullet 3"},
        {:text, ""},
        {:pre, %{title: "title"}, """
         _______________________
        #                       #
        | This is  preformatted |
        |  text with two lines  |
        #_______________________#
        """
        },
        {:text, ""}
      ]
  '''
  @spec parse(String.t()) :: gemtext_blocks()
  def parse(s) do
    case do_parse(s) do
      {:ok, acc, "" = _rest, %{} = _context, _line, _offset} -> acc
      err -> err
    end
  end
end
