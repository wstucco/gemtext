defmodule Gemtext.Parser.Blockquote do
  import NimbleParsec
  import Gemtext.Parser.Base

  def parser do
    concat(prefix(">"), body())
    |> reduce({Gemtext.Parser.Base, :cast, [:quote]})
  end
end
