defmodule Gemtext.Parser.Headers do
  import NimbleParsec
  import Gemtext.Parser.Base

  def parser do
    choice([h3(), h2(), h1()])
  end

  defp h1 do
    concat(prefix("#"), body())
    |> reduce({Gemtext.Parser.Base, :cast, [:h1]})
  end

  defp h2 do
    concat(prefix("##"), body())
    |> reduce({Gemtext.Parser.Base, :cast, [:h2]})
  end

  defp h3 do
    concat(prefix("###"), body())
    |> reduce({Gemtext.Parser.Base, :cast, [:h3]})
  end
end
