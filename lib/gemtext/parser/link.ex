defmodule Gemtext.Parser.Link do
  import NimbleParsec
  import Gemtext.Parser.Base

  def parser do
    prefix("=>")
    |> skip_white_spaces()
    |> token()
    |> skip_white_spaces()
    |> optional(non_empty_line())
    |> reduce({__MODULE__, :cast, []})
  end

  def cast([to]) do
    {:link, %{to: to}, nil}
  end

  def cast([to, body]) do
    {:link, %{to: to}, body}
  end
end
