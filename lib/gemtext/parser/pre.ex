defmodule Gemtext.Parser.Pre do
  import NimbleParsec
  import Gemtext.Parser.Base

  def parser do
    pre_start()
    |> concat(pre_body())
    |> concat(pre_end())
    |> reduce({__MODULE__, :cast, []})
  end

  def cast([body]) do
    {:pre, body}
  end

  def cast([title, body]) do
    {:pre, %{title: title}, body}
  end

  defp pre_start do
    prefix("```")
    |> skip_white_space()
    |> optional(non_empty_line())
    |> ignore(eol())
  end

  defp pre_body do
    repeat_while(concat(line(), eol()), {__MODULE__, :not_pre_end, []})
    |> reduce({:to_string, []})

    # |> unwrap_and_tag(:body)
  end

  defp pre_end do
    prefix("```")
    |> concat(line())
    |> ignore()
  end

  def not_pre_end(<<"```", _::binary>>, context, _, _) do
    {:halt, context}
  end

  def not_pre_end(_, context, _, _) do
    {:cont, context}
  end
end
