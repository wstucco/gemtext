defmodule Gemtext.Parser.Base do
  import NimbleParsec, except: [line: 1]

  def eol do
    choice([string("\r\n"), string("\r"), string("\n")])
  end

  def white_space do
    ascii_string([?\s, ?\t], 1)
    |> optional()
  end

  def skip_white_space do
    ignore(white_space())
  end

  def skip_white_space(combinator) do
    concat(combinator, skip_white_space())
  end

  def skip_white_spaces do
    ascii_string([?\s, ?\t], min: 0)
    |> ignore()
  end

  def skip_white_spaces(combinator) do
    concat(combinator, skip_white_spaces())
  end

  def line do
    utf8_line()
  end

  def line(combinator) do
    concat(combinator, line())
  end

  def non_empty_line do
    utf8_line(1)
  end

  defp utf8_line(min \\ 0) do
    utf8_string([not: ?\r, not: ?\n], min: min)
  end

  def token do
    skip_white_space()
    |> utf8_string([not: ?\s, not: ?\r, not: ?\n], min: 1)
  end

  def token(combinator) do
    concat(combinator, token())
  end

  def prefix(term) do
    term |> string() |> ignore()
  end

  def body do
    concat(skip_white_space(), line())
  end

  def body(combinator) do
    concat(combinator, body())
  end

  def cast([body], tag: tag, attrs: attrs) when is_list(attrs) do
    cast([body], tag: tag, attrs: Map.new(attrs))
  end

  def cast([body], tag: tag, attrs: attrs) when is_map(attrs) do
    {tag, attrs, body}
  end

  def cast([body], tag) do
    {tag, body}
  end
end
