defmodule Gemtext.Parser.Text do
  import NimbleParsec
  import Gemtext.Parser.Base

  def parser do
    line()
    |> reduce({Gemtext.Parser.Base, :cast, [:text]})
  end
end
