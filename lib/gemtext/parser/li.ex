defmodule Gemtext.Parser.Li do
  import NimbleParsec
  import Gemtext.Parser.Base

  def parser do
    prefix("*")
    |> concat(body())
    |> reduce({Gemtext.Parser.Base, :cast, [:li]})
  end
end
